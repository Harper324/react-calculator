import React from 'react';
import './calculator.less';

class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstNumber: '',
            secondNumber: '',
            calculateMethod: '',
            value: ''
        }

        this.add = this.add.bind(this);
        this.minus = this.minus.bind(this);
        this.multiply = this.multiply.bind(this);
        this.divide = this.divide.bind(this);
        this.getFirstNumber = this.getFirstNumber.bind(this);
        this.getSecondNumber = this.getSecondNumber.bind(this);
        this.zeroClean = this.zeroClean.bind(this);
        this.calculate = this.calculate.bind(this);

    }

    calculate() {
        switch (this.state.calculateMethod) {
            case '+':
                this.setState(this.state = {
                    value: this.state.firstNumber + this.state.secondNumber
                })
                break;
            case '-':
                this.setState(this.state = {
                    value: this.state.firstNumber - this.state.secondNumber
                })
                break;
            case '*':
                this.setState(this.state = {
                    value: this.state.firstNumber * this.state.secondNumber
                })
                break;
            case '/':
                this.setState(this.state = {
                    value: this.state.firstNumber / this.state.secondNumber
                })

                break;
        }
    }

    add() {
        this.setState(this.state = {
            calculateMethod: '+'
        })


    }

    minus() {
        this.setState(this.state = {
            calculateMethod: '-'
        })

    }

    multiply() {
        this.setState(this.state = {
            calculateMethod: '*'
        })

    }

    divide() {
        this.setState(this.state = {
            calculateMethod: '/'
        })

    }

    zeroClean() {
        this.setState(this.state = {
            firstNumber: '',
            secondNumber: '',
            value: ''
        })
    }

    getFirstNumber(event) {
        const number = parseFloat(event.target.value);
        this.setState(this.state = {firstNumber: number})
    }

    getSecondNumber(event) {
        const number = parseFloat(event.target.value);
        this.setState(this.state = {secondNumber: number})
    }


    render() {
        return (
            <section className="miniCalculator">
                <p>请输入数字
                    <input onChange={this.getFirstNumber} type="text" value={this.state.firstNumber}/>
                    <input onChange={this.getSecondNumber} type="text" value={this.state.secondNumber}/>
                </p>
                <div>计算结果为:
                    <span className="result">{this.state.value}</span>
                </div>
                <div className="operations">
                    <button onClick={this.add}>+</button>
                    <button onClick={this.minus}>-</button>
                    <button onClick={this.multiply}>*</button>
                    <button onClick={this.divide}>/</button>
                    <button onClick={this.zeroClean}>AC</button>
                    <button onClick={this.calculate}>=</button>
                </div>
            </section>
        );
    }


}

export default Calculator;

